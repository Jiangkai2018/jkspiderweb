from django.http import JsonResponse
from django.shortcuts import render


def show_index(request):
    context = {'msg': 'index信息', "title": "主页"}
    return render(request, 'index.html', context)
    # return JsonResponse(context)
