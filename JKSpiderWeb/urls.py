"""JKSpiderWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import show_index
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
  path('admin/', admin.site.urls),
  path('user/', include('Apps.userManage.urls')),
  path('articles/', include('Apps.articles.urls')),
  path('share/', include('Apps.share.urls')),
  path('tools/', include('Apps.tools.urls')),  path('ckeditor/', include('ckeditor_uploader.urls')),
  path('search/', include('haystack.urls')),  # 全文检索
  path('diySpider/', include('Apps.DIYSpider.urls')),  # 全文检索
  path('', show_index, name='index'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)  # 没有这一句无法显示上传的图片
