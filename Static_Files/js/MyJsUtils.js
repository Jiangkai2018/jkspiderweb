// 将KB，MB 转换成KB
var toTypes = function (ss) {
    var num = parseFloat(ss.substring(0, ss.length - 2))
    if (ss.match(RegExp(/GB/))) {
        num = num * 1024 * 1024 * 1024
    }
    if (ss.match(RegExp(/MB/))) {
        num = num * 1024 * 1024
    }
    if (ss.match(RegExp(/KB/))) {
        num = num * 1024
    }
    return num
};
// 将types 转换成 相应KB，MB
var toUint = function (ss) {
    var uType = 0;
    var dicList = ['B', 'KB', 'MB', 'GB', 'TB'];
    while (ss >= 1024) {
        ss = ss / 1024;
        uType += 1;
    }
    return ss.toFixed(2) + dicList[uType];
};
