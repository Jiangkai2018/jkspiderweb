$(document).ready(function () {
    // $('#testtable1').yhhDataTable();
    // 因为要做跳转，把第一个data做成链接即可


    if ($('input[name="pageType"]').val() == '0') {
        // 非收藏
        searchTableInit();
    }
    else {
        // 收藏
        param={'pageType':$('input[name="pageType"]').val()};
        searchTableInit(param);
    }

});
var preTable = ''
var searchTableInit = function (param) {
    if ($('div.data-table-top-box').length > 0) {
        // 已初始 : 还原之前代码，重新刷新表格
        $('.articlesList').html(preTable);
    }
    else {
        // 未初始化 : 备份原先前代码，直接刷新表格
        preTable = $('.articlesList').html().replace(/"\""/g, "\\\"");
    }
    url = "/articles/getArticleListsTable/";
    var testdata1 = []
    var page = $('#testtable2');
    $.get(url, param, function (data, success) {
        if (data) {
            var data = data['data'];
            for (var i = 0; i < data.length; i++) {
                var x = data[i];
                testdata1.push([x.titleLink, x.author, x.source, x.publishtime, x.readnum])
            }
            $('#testtable2').yhhDataTable({
                'paginate': {
                    'changeDisplayLen': true,
                    'type': 'updown',
                    'visibleGo': true
                },
                'tbodyRow': {
                    'zebra': true
                },
                'tbodyData': {
                    'enabled': true, /*是否传入表格数据*/
                    'source': testdata1 /*传入的表格数据*/
                }
            });

            // 刷新表格
            // console.log('testdata1,刷新数据为：', testdata1);
            // if (param) refreshTable(testdata1,1);

            /* 解決初始化表格闪烁问题 */
            $('#testtable2 > thead > tr').css('color', '#000000a1');
            /* 解決初始化表格闪烁问题 */
        }
        else {
            alert("接口查询失败！！", url);
        }
    });

    /*更新表格*/
    var refreshTable = function (data, page) {
        var $page = $('div.articlesList');
        if ($.isEmptyObject(data)) data = {};
        var toData = {
            'ajaxParam': {'data': data}
        }
        if (!$.isEmptyObject(page)) {
            toData.paginate = {};
            toData.paginate.currentPage = page;
        }
        var $table = $page.find('#testtable2');
        $table.yhhDataTable('refresh', toData);
    };

};


