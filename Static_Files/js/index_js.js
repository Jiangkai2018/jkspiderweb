$(function () {


    // nav收缩展开
    $('.navMenu li a').on('click', function () {
        var parent = $(this).parent().parent();//获取当前页签的父级的父级
        var labeul = $(this).parent("li").find(">ul")
        if ($(this).parent().hasClass('open') == false) {
            //展开未展开
            parent.find('ul').slideUp(300);
            parent.find("li").removeClass("open")
            parent.find('li a').removeClass("active").find(".arrow").removeClass("open")
            $(this).parent("li").addClass("open").find(labeul).slideDown(300);
            $(this).addClass("active").find(".arrow").addClass("open")
            $(parent).find("embed").attr('src', '/static/images/spider-solid.svg');
            $(this).find("embed").attr('src', '/static/images/spider-solid-white.svg');
        } else {
            $(parent).find("embed").attr('src', '/static/images/spider-solid.svg');
            $(this).find("embed").attr('src', '/static/images/spider-solid.svg');
            $(this).parent("li").removeClass("open").find(labeul).slideUp(300);
            if ($(this).parent().find("ul").length > 0) {
                $(this).removeClass("active").find(".arrow").removeClass("open")
            } else {
                $(this).addClass("active")
            }
        }
    });


    // 主页登录初始化：head_context 显示当前登录人

    var uname = $.cookie('username');
    console.log(uname);
    if (uname == undefined) {
        $('div.logined_info.fl').hide();
    } else {
        $('div.login_btn.fl > a').hide();
        $('div.logined_info.fl > em').html(uname);
    }
    ;
    //主页登录初始化end

    $(".navMenu li a").hover(function () {
        // 有open 不操作 离开不操作
        // 无open 放上加 离开移去
        // 放上
        if ($(this).parent().hasClass('open') == true) {
        }
        else {
            $(this).find("embed").attr('src', '/static/images/spider-solid-white.svg');
        }
        // 离开
    }, function () {
        if ($(this).parent().hasClass('open') == true) {
        }
        else {
            $(this).find("embed").attr('src', '/static/images/spider-solid.svg');
        }

    });

    // 全文检索回车触发
    $('.input_text').bind('keypress', function (event) {

        if (event.keyCode == "13") {
            search_index();
            return false;
        }
    });

});

var search_index = function () {
    console.log('hello');
    var findStr = $('div.search_con.fr > input.input_text.fl').val();
    console.log(findStr);
    window.location.href = "/search/?q="+findStr;

};