;$(function () {

    // 注册页
    // 注册页post请求
    // action="/user/registe_handle/" method="post"
    // registe_form
    // registe_submit

    $('.registe_submit').click(function () {
        var post_url = '/user/registe_handle/'
        var csrf = $('input[name="csrfmiddlewaretoken"]').val();
        var params = {
            'Username': $('.solid_heigth > input[type="text"]').val(),
            'email': $('.solid_heigth > input[type="email"]').val(),
            'password': $('.solid_heigth > input[name="password"]').val(),
            'password2': $('.solid_heigth > input[name="password2"]').val(),
            'csrfmiddlewaretoken': csrf
        };
        $.post(post_url, params, function (data) {
            // console.log(data);
            console.log(params);
            console.log(data.code);
            console.log(data.msg);
            if (data.code == '5') {
                var txt = data.msg;
                window.wxc.xcConfirm(txt, window.wxc.xcConfirm.typeEnum.success);
                // window.location.href=data.nextURL;
                // self.location.href=data.nextURL;  // 在先根级url 跳转
            }
            else {
                var txt = data.msg;
                window.wxc.xcConfirm(txt, window.wxc.xcConfirm.typeEnum.error);
            }
            ;
        });

    });

    // 登录页
    $('.login_submit').click(function () {
        var post_url = '/user/Login_handle/'
        var csrf = $('input[name="csrfmiddlewaretoken"]').val();
        var params = {
            'Username': $('#small-dialog1 input[name="Username"]').val(),
            'password': $('#small-dialog1 input[name="password"]').val(),
            'rememberTag': $('#small-dialog1 input[name="rememberTag"]').val(),
            'csrfmiddlewaretoken': csrf
        };
        $.post(post_url, params, function (data) {
            console.log(data);
            console.log(params);
            console.log(data.code);
            console.log(data.msg);
            if (data.code == '4') {
                // var txt= data.msg;
                // window.wxc.xcConfirm(txt, window.wxc.xcConfirm.typeEnum.success);
                window.location.href = data.nextURL;
                // self.location.href=data.nextURL;  // 在先根级url 跳转
            }
            else {
                var txt = data.msg;
                window.wxc.xcConfirm(txt, window.wxc.xcConfirm.typeEnum.error);
            }
            ;
        });
    });
    // 登录页end


    //保存信息页
    $("#savebtn").click(function () {
        console.log('1');
        // change_handle
        var post_url = "/user/saveUinfo/";
        params = {
            "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val(),
            "username": $('form > div:nth-child(1) > div > input').val(),
            "gender": $("input[name='gender']").val(),
            "password": $("form > div:nth-child(3) > div > input").val(),
            "password2": $("form > div:nth-child(4) > div > input").val(),
            "email": $("form > div:nth-child(5) > div > input").val(),
            "phone": $("form > div:nth-child(6) > div > input").val(),
            "qq": $("form > div:nth-child(7) > div > input").val(),
            "addr": $("form > div:nth-child(8) > div > input").val()
        };
        console.log(params);
        $.post(post_url, params, function (data) {
            if (data.code == '5') {
                var txt = data.msg;
                window.wxc.xcConfirm(txt, window.wxc.xcConfirm.typeEnum.success);
            }
            else {
                var txt = data.msg;
                window.wxc.xcConfirm(txt, window.wxc.xcConfirm.typeEnum.warning);
            }
            ;
        })
    });

    //信息修改页


    function uinfoinit() {
        $("div.btns > ul > li:nth-child(1)").show();
        $('body > div.total_content.clearfix > div.right_content.fl > div > div.userInfoForms').show();
        $('div.right_content.fl > div > form').hide();
        $('div.right_content.fl > div > div.btns > ul > li:nth-child(2)').hide();
        $('div.right_content.fl > div > div.btns > ul > li:nth-child(3)').hide();
    };
    uinfoinit();

    function change() {
        $("div.btns > ul > li:nth-child(1)").hide();
        $('body > div.total_content.clearfix > div.right_content.fl > div > div.userInfoForms').hide();
        $('div.right_content.fl > div > form').show();
        $('div.right_content.fl > div > div.btns > ul > li:nth-child(2)').show();
        $('div.right_content.fl > div > div.btns > ul > li:nth-child(3)').show();
    };
    // 修改
    $('div.btns > ul > li:nth-child(1) > a').click(function () {
        change();
    });
    // 取消
    $('div.btns > ul > li:nth-child(3) > a').click(function () {
        uinfoinit();
    })

    //判断用户输入的电子邮箱格式是否正确
    function checkEmail() {
        var myemail = $("form > div:nth-child(5) > div > input").val();
        var myReg = /^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/;

        if (myReg.test(myemail)) {
            return true;
        } else {
            window.wxc.xcConfirm("邮箱格式错误！", window.wxc.xcConfirm.typeEnum.error);
            return false;
        }
    }


});