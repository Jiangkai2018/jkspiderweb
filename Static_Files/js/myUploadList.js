/* 解決初始化表格闪烁问题 */
$('#testtable2 > thead > tr').css('color', '#fff0');


var class_dic = {
'1':'代码框架',
'2':'软件共享',
'3':'数据共享',
'4':'资料共享',
"":"",
};
var tabDic = {
    'tab1': 1,
    'tab2': 2,
    'tab3': 3,
    'tab4': 4,
    'tab':0,
};
var cName2tabNum = {
    '代码框架':'tab1',
    '软件共享':'tab2',
    '数据共享':'tab3',
    '资料共享':'tab4',
};



//页面载入初始化
$(document).ready(function () {
    commonInit();
});

var commonInit = function () {

    // 时间插件初始化
    $('.flatpickr').flatpickr();
    // 可编辑下拉列表初始化
    $('#editable-select').editableSelect({
        bg_iframe: true,
        case_sensitive: false,
        items_then_scroll: 10,
        isFilter: false
    });
    $('#editable-select2').editableSelect({
        bg_iframe: true,
        case_sensitive: false,
        items_then_scroll: 10,
        isFilter: false
    });
    // 单位初始化
    unitInit();
    // 搜索表格初始化
    searchTableInit(param = {'isForUploadList': '1'})
};

// 下拉框单位初始化
var unitInit = function () {
    var view = $(".optionBox li:first").text();
    $(".viewInput").val(view);
    $('.optionBox1').css("display", 'none');
    $('.optionBox2').css("display", 'none');
    var curTag = $(".tab-content")
    $(".viewBox").unbind('click').click(function () {
        var curParent = $(this).parent();
        curTag = curParent;
        if (curParent.find(".optionBox").css("display") == "none") {
            curParent.find(".optionBox").show();
            curParent.find("i.fa").removeClass('fa-caret-down').addClass('fa-caret-up')
        } else {
            curParent.find(".optionBox").hide();
            curParent.find("i.fa").removeClass('fa-caret-up').addClass('fa-caret-down')
        }
    });
    // 修改对应下拉框
    var checkOption = curTag.find(".optionBox li");
    checkOption.click(function () {
        var index = curTag.find(".optionBox li").index(this);
        var indexVal = curTag.find(".optionBox li:eq(" + index + ")").html();
        curTag.find(".viewInput").val(indexVal);
        if (curTag.find(".optionBox").css("display") == "none") {
            curTag.find(".optionBox").show();
            curTag.find(".btnImg img").attr("src", "arrow_input_up.png")
            curTag.find("body > div > div > i").removeClass('fa-caret-down').addClass('fa-caret-up')
        } else {
            curTag.find(".optionBox").hide();
            curTag.find(".btnImg img").attr("src", "arrow_input_down.png")
            curTag.find("body > div > div > i").removeClass('fa-caret-up').addClass('fa-caret-down')
        }
    })
};
// 检查大小合法性
var checkUnitDown = function () {
    var dic = {'KB': 0, 'MB': 1, 'GB': 2};
    var unit01 = dic[$('.viewBox1 input').val()];
    var unit02 = dic[$('.viewBox2 input').val()];
    var num01 = $('#size01').val();
    var num02 = $('#size02').val();
    num01 = num01 * Math.pow(1024, unit01);
    num02 = num02 * Math.pow(1024, unit02);

    var show = {
        'num01': num01,
        'num02': num02
    }
    console.log('show=', show);
    return (num01 >= num02) ? true : false;
};


var toTypes = function (ss) {
    var num = parseFloat(ss.substring(0, ss.length - 2))
    if (ss.match(RegExp(/GB/))) {
        num = num * 1024 * 1024 * 1024
    }
    if (ss.match(RegExp(/MB/))) {
        num = num * 1024 * 1024
    }
    if (ss.match(RegExp(/KB/))) {
        num = num * 1024
    }
    return num
};
// 将types 转换成 相应KB，MB
var toUint = function (ss) {
    var uType = 0;
    var dicList = ['B', 'KB', 'MB', 'GB', 'TB'];
    while (ss >= 1024) {
        ss = ss / 1024;
        uType += 1;
    }
    return ss.toFixed(2) + dicList[uType];
};

// 格式化string
String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (m, i) {
        return args[i];
    });
};

/* 重置表格条件 */
var resetTable = function () {
    $('#stitle').val("");
    $('#editable-select').val("");
    $('#editable-select2').val("");
    $('#size01').val("");
    $('#size02').val("");
    $('#stime').val(null);
    $('#etime').val(null);
    $('#star01').val("");
    $('#star02').val("");
    $('.viewBox1 .viewInput').val("KB");
    $('.viewBox2 .viewInput').val("KB");
}

/* 搜索我的上传整合参数 */
var searchShare = function () {
    sname = $('#stitle').val();
    class_name = $('#editable-select').val();
    size1 = $('#size01').val();
    size2 = $('#size02').val();
    publishtime_st = $('#stime').val();
    publishtime_et = $('#etime').val();
    star01 = $('#star01').val() * 10;
    star02 = $('#star02').val() * 10;

    unit01 = $('.viewBox1 .viewInput').val();
    unit02 = $('.viewBox2 .viewInput').val();
    console.log('unit01', unit01);
    size1 = toTypes(size1 + unit01);
    size2 = toTypes(size2 + unit02);
    var param = {};

    // 接口离别区分
    param['isForUploadList'] = '1'

    if (sname) {
        param['sname'] = sname
    }
    ;
    if (class_name) {
        class2curDiv = {
            '':""
        }
        param['tabNum'] = cName2tabNum[class_name]
    }
    ;
    if (size1) {
        param['size1'] = size1
    }
    ;
    if (size2) {
        param['size2'] = size2
    }
    ;
    if (publishtime_st) {
        param['publishtime_st'] = publishtime_st
    }
    ;
    if (publishtime_et) {
        param['publishtime_et'] = publishtime_et
    }
    ;
    if (star01) {
        param['star01'] = star01
    }
    ;
    if (star02) {
        param['star02'] = star02
    }
    ;
    console.log('AAAsearchTableInit.param', param);
    searchTableInit(param = param);
};

var preTable = "";

// 初次打开页面，不带参数，查所有
// 传参为条件查询

var searchTableInit = function (param) {
    console.log('查询前参数/param:', param);
    if ($('div.data-table-top-box').length > 0) {
        // 已初始 : 还原之前代码，重新刷新表格
        $('.articlesList').html(preTable);
    }
    else {
        // 未初始化 : 备份原先前代码，直接刷新表格
        preTable = $('.articlesList').html().replace(/"\""/g, "\\\"");
    }
    url = "/share/findShareListTable/";
    var testdata = []
    // var page = $('#testtable2');
    var page = $('#testtable2');


    $.get(url, param, function (data, success) {
        console.log('接收到的数据：', data)
        if (data) {
            var dataList = data['DATALists'];
            var DATA_Length = data['DATA_Length'];
            for (var i = 0; i < dataList.length; i++) {
                var x = dataList[i];
                var class_name = class_dic[x.class_id]
                titleLink = '<a href=\"/share/getShareInfoPage/' + x.share_id + '/\">' + x.share_name + '</a>'
                var star = DATA_Length == 0 || DATA_Length == undefined ? "" : (x.star / 10).toFixed(1)
                testdata.push([titleLink, class_name, x.file_size, x.update_time, star])
            }
            $('#testtable2').yhhDataTable({
                'paginate': {
                    'changeDisplayLen': true,
                    'type': 'updown',
                    'visibleGo': true
                },
                'tbodyRow': {
                    'zebra': true
                },
                'tbodyData': {
                    'enabled': true, /*是否传入表格数据*/
                    'source': testdata /*传入的表格数据*/
                }
            });

            // 刷新表格
            // console.log('testdata1,刷新数据为：', testdata1);
            // if (param) refreshTable(testdata1,1);

            /* 解決初始化表格闪烁问题 */
            $('#testtable2 > thead > tr').css('color', '#000000a1');
            /* 解決初始化表格闪烁问题 */
        }
        else {
            alert("接口查询失败！！", url);
        }
    });

    /*更新表格*/
    var refreshTable = function (data, page) {
        var $page = $('div.articlesList');
        if ($.isEmptyObject(data)) data = {};
        var toData = {
            'ajaxParam': {'data': data}
        }
        if (!$.isEmptyObject(page)) {
            toData.paginate = {};
            toData.paginate.currentPage = page;
        }
        var $table = $page.find('#testtable2');
        $table.yhhDataTable('refresh', toData);
    };
};

