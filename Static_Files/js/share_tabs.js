/* 解決初始化表格闪烁问题 */
$('#testtable2 > thead > tr').css('color', '#fff0');

//页面载入初始化
// 根据不同标签已触发的tabs初始化
$(document).ready(function () {

    $(".tab-content").hide();
    $("ul.tab-head li:first").addClass("active").show();
    $(".tab-content:first").show();
    var activeTab = $('li.active').find("a").attr("name");
    commonInit(activeTab);
    //单击事件
    $("ul.tab-head li").click(function () {
        $("ul.tab-head li").removeClass("active");
        $(this).addClass("active");
        $(".tab-content").hide();

        activeTab = $(this).find("a").attr("name");
        $(activeTab).fadeIn();
        // 相应的激活
        commonInit(activeTab);
        return false;
    });
});

var commonInit = function (curDiv) {

    // 时间插件初始化
    console.log('curDiv01:', curDiv);
    $(curDiv).find('.flatpickr').flatpickr();
    // 可编辑下拉列表初始化
    $(curDiv).find('#editable-select').editableSelect({
        bg_iframe: true,
        case_sensitive: false,
        items_then_scroll: 10,
        isFilter: false
    });
    $(curDiv).find('#editable-select2').editableSelect({
        bg_iframe: true,
        case_sensitive: false,
        items_then_scroll: 10,
        isFilter: false
    });
    // 下拉列表初始化
    searchShareListInit(curDiv);
    // 单位初始化
    unitInit(curDiv);
    // 搜索表格初始化
    console.log('搜索表格初始化：' + curDiv);
    searchTableInit(curDiv = curDiv, param = {'tabNum': curDiv.substring(1)})
};

// 下拉框单位初始化
var unitInit = function (curDiv) {
    var view = $(curDiv).find(".optionBox li:first").text();
    $(curDiv).find(".viewInput").val(view);
    $(curDiv).find('.optionBox1').css("display", 'none');
    $(curDiv).find('.optionBox2').css("display", 'none');
    var curTag = $(curDiv);
    $(curDiv).find(".viewBox").unbind('click').click(function () {
        var curParent = $(this).parent();
        curTag = curParent;
        if (curParent.find(".optionBox").css("display") == "none") {
            $('.optionBox').hide();
            curParent.find(".optionBox").show();
            curParent.find("i.fa").removeClass('fa-caret-down').addClass('fa-caret-up')
        } else {
            curParent.find(".optionBox").hide();
            curParent.find("i.fa").removeClass('fa-caret-up').addClass('fa-caret-down')
        }
    });
    // 修改对应下拉框
    var checkOption = curTag.find(".optionBox li");
    checkOption.click(function () {
        var index = curTag.find(".optionBox li").index(this);
        var indexVal = curTag.find(".optionBox li:eq(" + index + ")").html();
        curTag.find(".viewInput").val(indexVal);
        if (curTag.find(".optionBox").css("display") == "none") {
            curTag.find(".optionBox").show();
            curTag.find(".btnImg img").attr("src", "arrow_input_up.png")
            curTag.find("body > div > div > i").removeClass('fa-caret-down').addClass('fa-caret-up')
        } else {
            curTag.find(".optionBox").hide();
            curTag.find(".btnImg img").attr("src", "arrow_input_down.png")
            curTag.find("body > div > div > i").removeClass('fa-caret-up').addClass('fa-caret-down')
        }
    })
};
// 检查大小合法性
var checkUnitDown = function () {
    var dic = {'KB': 0, 'MB': 1, 'GB': 2};
    var unit01 = dic[$('.viewBox1 input').val()];
    var unit02 = dic[$('.viewBox2 input').val()];
    var num01 = $('#size01').val();
    var num02 = $('#size02').val();
    num01 = num01 * Math.pow(1024, unit01);
    num02 = num02 * Math.pow(1024, unit02);

    var show = {
        'num01': num01,
        'num02': num02
    }
    console.log('show=', show);
    return (num01 >= num02) ? true : false;
};


// 搜索下拉列表初始化
var searchShareListInit = function (curDiv) {

    if ($(curDiv).find('ul.es-list').find('li').length > 0) {
        return;
    }
    console.log('curDiv0311：', curDiv);
    url = "/share/getTabsDownList/";
    param = {
        'findLists': 'update_user_id',
        'tabNum': curDiv.substring(1)
    };
    $.get(url, param, function (res, success) {
        console.log(res);
        var datalist = res.data.update_user_id;
        for (var i = 0; i < datalist.length; i++) {
            $(curDiv).find('#editable-select').editableSelect('add', datalist[i], i);
        }
        ;
    });
};
var toTypes = function (ss) {
    var num = parseFloat(ss.substring(0, ss.length - 2))
    if (ss.match(RegExp(/GB/))) {
        num = num * 1024 * 1024 * 1024
    }
    if (ss.match(RegExp(/MB/))) {
        num = num * 1024 * 1024
    }
    if (ss.match(RegExp(/KB/))) {
        num = num * 1024
    }
    return num
};
// 将types 转换成 相应KB，MB
var toUint = function (ss) {
    var uType = 0;
    var dicList = ['B', 'KB', 'MB', 'GB', 'TB'];
    while (ss >= 1024) {
        ss = ss / 1024;
        uType += 1;
    }
    return ss.toFixed(2) + dicList[uType];
};

// 格式化string
String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (m, i) {
        return args[i];
    });
};

/* 重置表格条件 */
var resetTable = function (curDiv) {
    $(curDiv).find('#stitle').val("");
    $(curDiv).find('#editable-select').val("");
    $(curDiv).find('#editable-select2').val("");
    $(curDiv).find('#size01').val("");
    $(curDiv).find('#size02').val("");
    $(curDiv).find('#stime').val(null);
    $(curDiv).find('#etime').val(null);
    $(curDiv).find('#star01').val("");
    $(curDiv).find('#star02').val("");
    $(curDiv).find('.viewBox1 .viewInput').val("KB");
    $(curDiv).find('.viewBox2 .viewInput').val("KB");
}
/* 搜索分享列表 */
var searchShare = function (curDiv) {
    console.log('jk0123');
    console.log('this:', curDiv);
    tabNum = curDiv.substring(1);
    sname = $(curDiv).find('#stitle').val();
    suser = $(curDiv).find('#editable-select').val();
    size1 = $(curDiv).find('#size01').val();
    size2 = $(curDiv).find('#size02').val();
    publishtime_st = $(curDiv).find('#stime').val();
    publishtime_et = $(curDiv).find('#etime').val();
    star01 = $(curDiv).find('#star01').val()*10;
    star02 = $(curDiv).find('#star02').val()*10;

    unit01 = $(curDiv).find('.viewBox1 .viewInput').val();
    unit02 = $(curDiv).find('.viewBox2 .viewInput').val();
    console.log('unit01', unit01);
    size1 = toTypes(size1 + unit01);
    size2 = toTypes(size2 + unit02);
    var param = {};
    if (tabNum) {
        param['tabNum'] = tabNum
    }

    if (sname) {
        param['sname'] = sname
    }
    ;
    if (suser) {
        param['suser'] = suser
    }
    ;
    if (size1) {
        param['size1'] = size1
    }
    ;
    if (size2) {
        param['size2'] = size2
    }
    ;
    if (publishtime_st) {
        param['publishtime_st'] = publishtime_st
    }
    ;
    if (publishtime_et) {
        param['publishtime_et'] = publishtime_et
    }
    ;
    if (star01) {
        param['star01'] = star01
    }
    ;
    if (star02) {
        param['star02'] = star02
    }
    ;
    console.log('AAAsearchTableInit.curDiv', curDiv);
    console.log('AAAsearchTableInit.param', param);
    searchTableInit(curDiv, param = param);
};

var preTable = {};

// 初次打开页面，不带参数，查所有
// 传参为条件查询
var searchTableInit = function (curDiv, param) {
    console.log('查询前参数/param:', param);
    if ($(curDiv).find('div.data-table-top-box').length > 0) {
        // 已初始 : 还原之前代码，重新刷新表格
        $(curDiv).find('.articlesList').html(preTable[curDiv]);
    }
    else {
        // 未初始化 : 备份原先前代码，直接刷新表格
        preTable[curDiv] = $(curDiv).find('.articlesList').html().replace(/"\""/g, "\\\"");
    }
    url = "/share/findShareListTable/";
    var testdata = []
    // var page = $('#testtable2');
    var page = $(curDiv).find('#testtable2');

    $.get(url, param, function (data, success) {
        console.log('接收到的数据：',data)
        if (data) {
            var dataList = data['DATALists'];
            var DATA_Length = data['DATA_Length'];
            for (var i = 0; i < dataList.length; i++) {
                var x = dataList[i];
                titleLink = '<a href=\"/share/getShareInfoPage/' + x.share_id + '/\">' + x.share_name + '</a>'
                var star =DATA_Length==0||DATA_Length==undefined?"":(x.star/10).toFixed(1)
                testdata.push([titleLink, x.userName, x.file_size, x.update_time, star])
            }
            $(curDiv).find('#testtable2').yhhDataTable({
                'paginate': {
                    'changeDisplayLen': true,
                    'type': 'updown',
                    'visibleGo': true
                },
                'tbodyRow': {
                    'zebra': true
                },
                'tbodyData': {
                    'enabled': true, /*是否传入表格数据*/
                    'source': testdata /*传入的表格数据*/
                }
            });

            // 刷新表格
            // console.log('testdata1,刷新数据为：', testdata1);
            // if (param) refreshTable(testdata1,1);

            /* 解決初始化表格闪烁问题 */
            $('#testtable2 > thead > tr').css('color', '#000000a1');
            /* 解決初始化表格闪烁问题 */
        }
        else {
            alert("接口查询失败！！", url);
        }
    });

    /*更新表格*/
    var refreshTable = function (data, page) {
        var $page = $('div.articlesList');
        if ($.isEmptyObject(data)) data = {};
        var toData = {
            'ajaxParam': {'data': data}
        }
        if (!$.isEmptyObject(page)) {
            toData.paginate = {};
            toData.paginate.currentPage = page;
        }
        var $table = $page.find('#testtable2');
        $table.yhhDataTable('refresh', toData);
    };
};

