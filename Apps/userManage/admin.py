from django.contrib import admin
from .models import *
# Register your models here.




@admin.register(UserInfo)
class userAdmin(admin.ModelAdmin):

    list_display = ['id', 'username', 'password', 'email', 'phone', 'address', 'qq', 'lastLoginTime', 'isdelete', 'gender']
    list_filter = ['username', 'isdelete']
    # list_per_page设置每页显示多少条记录，默认是100条
    list_per_page = 20
    ordering = ('id',)
    # 设置哪些字段可以点击进入编辑界面
    search_fields = ['username']
    list_display_links = ('id', 'username')



admin.site.site_header = '基于Django的网络爬虫助手网站后台系统'
admin.site.site_title = '|基于Django的网络爬虫助手网站'