from _sha1 import sha1

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from Apps.userManage import user_login
from Apps.userManage.models import UserInfo
import re

# config
index_url = "/"


def Login_register(request):
    return render(request, 'userManage/Login_register.html')


def Login_handle(request):
    if request.method == 'POST':
        username = request.POST.get('Username')
        password = request.POST.get('password')
        rememberTag = request.POST.get('rememberTag', 0)
        if not all([username, password]):
            return JsonResponse({"code": 1, "msg": "参数不完整，请检查提交注册信息"})

        try:
            user = UserInfo.objects.get(username=username)
        except UserInfo.DoesNotExist:
            user = None
        if user is not None:
            # 用户存在
            sha = sha1()
            sha.update(password.encode('utf8'))
            encrypwd = sha.hexdigest()
            if user.password == encrypwd:
                url = request.COOKIES.get('url')
                index_url = url or "/"
                res = JsonResponse({"code": 4, "msg": "用户登录验证成功", "nextURL": index_url})
                if rememberTag:
                    res.set_cookie('username', username)
                else:
                    res.set_cookie('username', '', max_age=-1)
                request.session['user_id'] = user.id
                request.session['username'] = username
                return res
            else:
                return JsonResponse({"code": 3, "msg": "密码错误，请重新填写"})
        else:
            # 用户不存在
            return JsonResponse({"code": 2, "msg": "用户不存在，请先注册再登录"})
    else:
        return JsonResponse({"code": 5, "msg": "请使用post方法访问"})


def registe_handle(request):
    if request.method == 'POST':
        username = request.POST.get('Username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')

        if not all([username, email, password, password2]):
            return JsonResponse({"code": 1, "msg": "参数不完整，请检查提交注册信息"})

        if password != password2:
            return JsonResponse({"code": 2, "msg": "密码不一致,请保持提交密码一致"})

        # 校验用户名是否已注册
        try:
            user = UserInfo.objects.get(username=username)
        except UserInfo.DoesNotExist:
            user = None
        if user is not None:
            return JsonResponse({"code": 3, "msg": "用户已存在，请重新输入"})

        # 密码加密存储
        sha = sha1()
        sha.update(password.encode('utf8'))
        encrypwd = sha.hexdigest()

        user = UserInfo()
        user.username = username
        user.email = email
        user.password = encrypwd
        user.save()
        return JsonResponse({"code": 5, "msg": "成功注册保存", "nextURL": index_url})
    else:
        return JsonResponse({"code": 4, "msg": "请使用post方法访问"})

def logout(request):
    request.session.flush()
    red = redirect('/user/')
    red.delete_cookie('username')
    return red

# 登录状态信息
def getBaseInfo(request):
    user_id = request.session.get('user_id')
    if user_id:
        userinfo = UserInfo.objects.get(pk=user_id)
        userinfojson = {
            "username": userinfo.username,
            "gender": userinfo.gender,
            "email": userinfo.email,
            "phone": userinfo.phone,
            "address": userinfo.address,
            "qq": userinfo.qq,
        }
    else:
        userinfojson = {
            "username": "游客",
            "gender": "未知性别",
            "email": "未知email",
            "phone": "未知电话",
            "address":"未知地址",
            "qq": "未知QQ",
        }

    context = {'code': 1, "msg": "测试ok",
               "userinfo": userinfojson,
               }
    return context


# 修改信息
@user_login.login
def changeUinfo(request):
    context = getBaseInfo(request)
    update = {'title': "学习网站——个人信息修改"}
    context.update(update)
    # return JsonResponse(context)
    return render(request, 'userManage/changeUinfo.html', context)

def about(request):
    context = getBaseInfo(request)
    update = {'title': "关于"}
    context.update(update)
    # return JsonResponse(context)
    return render(request, 'userManage/about.html', context)
@user_login.login
def saveUinfo(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        gender = request.POST.get('gender')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        qq = request.POST.get('qq')
        addr = request.POST.get('addr')
        if not all([username, email, password, password2]):
            return JsonResponse({"code": 1, "msg": "关键信息不完整，请检查提交注册信息"})
        if password != password2:
            return JsonResponse({"code": 2, "msg": "密码不一致,请保持提交密码一致"})

        if len(email) > 7:
            if re.match("^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$", email) != None:
                pass
            else:
                return JsonResponse({"code": 6, "msg": "邮箱格式错误，请经常"})
        else:
            return JsonResponse({"code": 6, "msg": "邮箱格式错误，请经常"})
        try:
            user = UserInfo.objects.get(username=username)
        except UserInfo.DoesNotExist:
            user = None
            return JsonResponse({"code": 3, "msg": "用户不存在，请重新输入"})
        else:
            user.username = username
            user.gender = gender

            sha = sha1()
            sha.update(password.encode('utf8'))
            encrypwd = sha.hexdigest()
            user.password = encrypwd

            user.email = email
            user.address = addr
            user.isdelete = False
            user.phone = phone
            user.qq = qq
            user.save()
            return JsonResponse({"code": 5, "msg": "已成功修改信息"})
    else:
        return JsonResponse({"code": 4, "msg": "请使用post方法访问"})
