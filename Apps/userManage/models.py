from django.db import models


# Create your models here.
class UserInfo(models.Model):
    username = models.CharField(max_length=20)
    gender = models.BooleanField(default=True,blank=True)
    password = models.CharField(max_length=40)
    email = models.CharField(max_length=50, default="",blank=False)
    phone = models.CharField(max_length=12, default="",blank=True)
    address = models.CharField(max_length=100, default="",blank=True)
    qq = models.CharField(max_length=50, default="",blank=True)
    lastLoginTime = models.TimeField(auto_now=True)  # 最近登录时间
    isdelete = models.BooleanField(default=False)
    def __str__(self):
        if self.username:
            return self.username
        else:
            return "无"