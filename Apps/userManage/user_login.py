# coding=utf-8

from django.shortcuts import redirect
from django.http import HttpResponseRedirect

# 如果未登陆，则跳转到登陆页
def login(func):
    def login_fun(request, *args, **kargs):
        if request.session.has_key('user_id'):
            return func(request, *args, **kargs)
        else:
            redirect = HttpResponseRedirect('/user/')
            redirect.set_cookie('url', request.get_full_path(),max_age=120)  # 将全路径存放到cookies中
            return redirect
    return login_fun
