from django.urls import path
from . import views

# /user/logout/
urlpatterns = [
    path('', views.Login_register, name='Login_register'),
    path('registe_handle/', views.registe_handle, name='registe_handle'),
    path('Login_handle/', views.Login_handle, name='Login_handle'),
    path('logout/', views.logout, name='logout'),
    path('changeUinfo/', views.changeUinfo, name='changeUinfo'),
    path('saveUinfo/', views.saveUinfo, name='saveUinfo'),
    path('about/', views.about, name='about'),
]
