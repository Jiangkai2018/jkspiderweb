# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from ckeditor.fields import RichTextField


# Unable to inspect table 'table'
# The error was: (1146, "Table 'jkspiderweb.table' doesn't exist")

# 共享信息表
class Shares(models.Model):
    share_id = models.AutoField(primary_key=True)
    update_user_id = models.IntegerField(blank=True, null=True)
    class_id = models.IntegerField()
    share_name = models.CharField(max_length=255, blank=True, null=True)
    star = models.IntegerField(blank=True, null=True)
    logo_img = models.CharField(max_length=255, blank=True, null=True)
    version = models.CharField(max_length=255, blank=True, null=True)
    file_size = models.CharField(max_length=20, blank=True, null=True)
    file_size2 = models.FloatField(blank=True, null=True)
    file_url = models.CharField(max_length=255, blank=True, null=True)
    sup_sys_type = models.CharField(max_length=255, blank=True, null=True)
    sup_sys_digits = models.CharField(max_length=20, blank=True, null=True)
    # content = models.TextField(blank=True, null=True)
    content = RichTextField('内容', config_name='my_config')
    update_time = models.DateTimeField(blank=True, null=True)
    is_sys_upload = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'shares'

# 共享文件展示图片表
class SharePicture(models.Model):
    sp_pk = models.AutoField(primary_key=True)
    share_id = models.IntegerField(blank=True, null=True)
    pic_url = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'share_picture'


# 删除文件记录表
class CommonfileserviceFile(models.Model):
    file = models.CharField(max_length=500, blank=True, null=True)
    real_name = models.CharField(db_column='real_Name', max_length=200)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'commonfileservice_file'


# 点赞评分表
class LikeGrade(models.Model):
    like_grade_id = models.AutoField(primary_key=True)
    shares_id = models.ForeignKey(to=Shares,on_delete=models.CASCADE, default='')
    base_score = models.IntegerField(blank=True, null=True)
    like_num = models.IntegerField(blank=True, null=True)
    nlike_num = models.IntegerField(blank=True, null=True)
    final_score = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'like_grade'
