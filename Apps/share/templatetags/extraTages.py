from django import template

register = template.Library()

@register.filter
def multiplyBy(x,y):
    return x*y

@register.filter
def searchmultiplyBy(x,y):
    return (x-1)*y


@register.simple_tag
def multiplyByTag(x,y):
    return x+y

@register.filter
def devideBy(x,y):
    return round(x/y,2)

@register.simple_tag
def devideByTag(x,y,yushu=1):
    print('x={},y={},res = {}',x,y,round(x/y,yushu))
    return round(x/y,yushu)
