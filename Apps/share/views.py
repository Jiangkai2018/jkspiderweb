import os

from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.forms import model_to_dict
from django.http import JsonResponse
from django.shortcuts import render
from django.core import serializers
# Create your views here.
from django.utils.safestring import mark_safe

from Apps.JkUtils.utils import paramDealNull, parseUrl, beUint
from Apps.share.models import *
from Apps.userManage import user_login
from Apps.userManage.models import UserInfo
from Apps.userManage.views import getBaseInfo

# get页面
from JKSpiderWeb.settings import *

tabDic = {
    'tab1': 1,
    'tab2': 2,
    'tab3': 3,
    'tab4': 4,
    'tab': 0,
}

@user_login.login
def getShareList(request):
    context = getBaseInfo(request)
    update = {'title': "分享中心"}
    context.update(update)
    return render(request, 'shares/sharesIndex.html', context)


# 共享详情页
@user_login.login
def getShareInfoPage(request, sid):
    # test: 339
    context = getBaseInfo(request)
    update = {'title': "分享详情"}
    print('sid=', sid)
    context.update(update)
    try:
        s = Shares.objects.get(pk=sid)
        content = mark_safe(s.content)
    except Shares.DoesNotExist:
        return JsonResponse({'err': 0, 'errmsg': '参数不正确，或者查询数据不存在', 'Exception': 'test'})
        content = """<div class="detail-desc"><div class="cont-title">软件简介</div><div class="cont-content"><p>暂无软件介绍</p></div>"""
    shareInfo = model_to_dict(s)
    shareInfo['content'] = content
    # picLists = SharePicture.objects.filter(share_id=sid)
    picLists = SharePicture.objects.filter(share_id=sid)
    picLists = serializers.serialize("json", picLists)
    update02 = {'shareInfo': shareInfo}
    update02['shareInfo'].update({"picLists": eval(picLists)})
    context.update(update02)
    # return JsonResponse(context)
    return render(request, 'shares/shareInfo.html', context)


def getShareUpload(request):
    return render(request, 'shares/testCke.html')


# 我要上传页面
@user_login.login
def getUploadPage(request):
    return render(request, 'shares/uploadPage.html')


# 我的上传页面
@user_login.login
def getMyUploadList(request):
    return render(request, 'shares/myUploadList.html')


# 我要上传信息
def uploadInfo(request):
    content = {'msg', '测试01'}
    return JsonResponse(content)


# 上传接口(文件+照片+Logo）写入文件信息cookies
def fileUpload(request):
    if request.method == "POST":
        file_obj = request.FILES.get('file')
        filename = file_obj.name
        fileSize = file_obj.size
        if file_obj:
            res = JsonResponse({'msg:': '上传成功', 'code': 200})
        # 0 文件Logo，1 多文件截图列表 、 2 文件
        # 写入cookie:p03、p09、p10、p12
        my_dic = {'0': 'p03', '1': 'p09', '2': ['p10', 'p12']}
        myFileType = request.POST.get('fileType')
        if myFileType == '0':
            filepath = os.path.join(MEDIA_ROOT, IMG_PATH, filename)
            fileURL = parseUrl(filepath, MEDIA_ROOT)
            print('整改后：', filepath)
            res.set_cookie("p03", fileURL.encode('utf-8'), max_age=900)
        elif myFileType == '1':
            filepath = os.path.join(MEDIA_ROOT, IMG_PATH, filename)
            fileURL = parseUrl(filepath, MEDIA_ROOT)
            cookie_key = my_dic[myFileType]
            pre_cookie = request.COOKIES.get(cookie_key)
            if pre_cookie:
                fileURL = eval(pre_cookie).decode('utf-8') + ',' + fileURL
            print('写入filepath:', filepath)
            res.set_cookie("p09", fileURL.encode('utf-8'), max_age=900)
            filepath = filepath.split(',')[-1]
        else:
            filepath = os.path.join(MEDIA_ROOT, CKEDITOR_UPLOAD_PATH, filename)
            fileURL = parseUrl(filepath, MEDIA_ROOT)
            fileUrl = ''
            res.set_cookie("p10", fileURL.encode('utf-8'), max_age=900)
            res.set_cookie("p12", fileSize, max_age=900)
        from django.core.files.storage import default_storage
        print('filepath:', filepath)
        update_fileservice(filepath, filename)
        with default_storage.open(filepath, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)
        return res


def update_fileservice(file, realname):
    '''
    对公共文件表插入文件数据
    :param file:
    :param realname:
    :return:
    '''
    if all([file, realname]):
        CommonfileserviceFile.objects.create(file=file, real_name=realname)
    else:
        return {'errmsg', '参数不全！'}


def findShareListTable(request):
    '''
    表格初始化、多条件查询 、 我的上传
    :param request:
    :return:
    '''

    if request.method == "GET":
        tabNum = request.GET.get('tabNum', default='tab')  # tab 为全类型查询
        isForUploadList = request.GET.get('isForUploadList', default="")
        share_id = request.GET.get('share_id', default='')
        name = request.GET.get('sname', default='')
        author = request.GET.get('suser', default='')
        s1 = request.GET.get('size1', default=-1)
        s2 = request.GET.get('size2', default=2147483647)
        publishtime_st = request.GET.get('publishtime_st', default='1970-01-01 00:00:00')
        publishtime_et = request.GET.get('publishtime_et', default='2099-01-01 00:00:00')
        readnum_sn = request.GET.get('star01', default=-1)
        readnum_en = request.GET.get('star02', default=2147483647)

        if author:
            try:
                user = UserInfo.objects.get(username=author)
                user_id = user.id
            except ObjectDoesNotExist:
                user_id = 0
        else:
            user_id = ''

        if isForUploadList:
            try:
                user_id = request.session['user_id']
            except Exception as e:
                return {'err': 0, 'DATA_Length': 0, 'errmsg': '参数不正确，请先登录后，在访问个人发布页面'}

        findparam = {
            'class_id': tabDic[tabNum],
            'share_id': share_id,
            'share_name__contains': name,
            'update_user_id': user_id,
            'file_size2__range': [s1, s2],
            'update_time__range': [publishtime_st, publishtime_et],
            'star__range': [readnum_sn, readnum_en],
        }
        findparam = paramDealNull(findparam)
        try:
            SharListTables = Shares.objects.filter(**findparam)
        except Exception:
            ret = {'err': 0, 'DATA_Length': 0, 'errmsg': '参数不正确，或者查询数据不存在', 'Exception': 'test'}
            return JsonResponse(ret)
            # 序列化
        resdata = [model_to_dict(Articles) for Articles in SharListTables]
        SharListTablesInfo = []

        NullObj = {
            "share_id": "",
            "update_user_id": "",
            "class_id": "",
            "share_name": "",
            "star": "",
            "logo_img": "",
            "version": "",
            "file_size": "",
            "file_size2": "",
            "file_url": "",
            "sup_sys_type": "",
            "sup_sys_digits": "",
            "content": "",
            "update_time": "",
            "userName": ""
        }
        if len(resdata):
            for x in resdata:
                username = UserInfo.objects.get(pk=x['update_user_id']).username
                x.update({'userName': username})
                SharListTablesInfo.append(x)

            data = {'code': 200,
                    'msg': '查询成功！',
                    'param': findparam,
                    'DATA_Length': len(SharListTablesInfo),
                    'DATALists': SharListTablesInfo,
                    }
            return JsonResponse(data)
        else:
            SharListTablesInfo.append(NullObj)
            data = {'code': 200,
                    'msg': '未查询到相关信息！',
                    'param': findparam,
                    'DATA_Length': 0,
                    'DATALists': SharListTablesInfo,
                    }
            return JsonResponse(data)
    else:
        ret = {'err': 1, 'errmsg': '请求方法不正确，请使用get'}
        return JsonResponse(ret)


def getTabsDownList(request):
    '''
    公共下拉列表接口
    :param: findLists 下拉列表字段列表，tabNum 所属tab编号
    :return: JsonResponse
    '''

    if request.method == "GET":
        findLists = request.GET.get('findLists', default='').split(',')
        tabNum = request.GET.get('tabNum', default='0')
        data = {}

        So = Shares.objects
        if tabNum:
            class_id = tabDic[tabNum]
            Ao = So.filter(class_id=class_id)

        # 对应返回对象的可显示字段列表
        if len(findLists) > 0:
            # findLists :['author', 'source']
            for o in findLists:
                r = Ao.values(o).distinct().order_by(o)
                rs = []
                for key in r:
                    if o == 'update_user_id':
                        v = UserInfo.objects.get(pk=key[o]).username
                    else:
                        v = key[o]
                    rs.append(v)
                # 方法二：
                # rs = json.dumps(list(r), cls=DjangoJSONEncoder)
                data.update({o: rs})

        context = {
            'code': 200, "msg": "查询成功！",
            'data': data,
        }
        return JsonResponse(context)  # test
    else:
        return JsonResponse({'err': 1, 'errmsg': '请求方法不正确，请使用get'})


# 图片上传、文件上传
def imageFileUpload(request):
    context = {
        'code': 200, "msg": "上传测试！",
        # 'data': data,
    }
    return JsonResponse(context)  # test


# 文件信息上传
def uploadInfo(request):
    try:
        user_id = request.session['user_id']
    except KeyError:
        return JsonResponse({'code': 1, 'errmsg': '请先登录再上传'})

    if request.method == 'GET':
        return JsonResponse({'res': 2, 'errmsg': '请使用POST提交'})
    classType = request.POST.get('classType', '')
    share_name = request.POST.get('share_name', '')
    star = request.POST.get('star', '')
    logo_img = eval(request.POST.get('logo_img', '')).decode('utf-8')
    version = request.POST.get('version', '')
    file_size = request.POST.get('file_size', '1024')
    file_url = eval(request.POST.get('file_url', '')).decode('utf-8')
    sup_sys_type = request.POST.get('sup_sys_type', '')
    sup_sys_digits = request.POST.get('sup_sys_digits', '')
    pic_list = eval(request.POST.get('pic_list', '')).decode('utf-8').split(',')
    content = request.POST.get('content', '')
    update_time = request.POST.get('update_time', '')
    is_sys_upload = request.POST.get('is_sys_upload', '')
    # 保存数据到数据库
    SharesParam = {
        "update_user_id": user_id,
        "class_id": classType,
        "share_name": share_name,
        "star": star,
        "logo_img": logo_img,
        "version": version,
        "file_size": beUint(file_size),
        "file_size2": file_size,
        "file_url": file_url,
        "sup_sys_type": sup_sys_type,
        "sup_sys_digits": sup_sys_digits,
        "content": content,
        "update_time": update_time,
        "is_sys_upload": is_sys_upload,
    }
    sharesobj = Shares.objects.create(**SharesParam)
    for url in pic_list:
        SharePicture.objects.create(share_id=sharesobj.pk, pic_url=url)

    params = {
        "classType": classType,
        "share_name": share_name,
        "star": star,
        "logo_img": logo_img,
        "version": version,
        "file_size": file_size,
        "file_url": file_url,
        "sup_sys_type": sup_sys_type,
        "sup_sys_digits": sup_sys_digits,
        "pic_list": pic_list,
        "content": content,
        "update_time": update_time,
        "is_sys_upload": is_sys_upload,
    }

    content = {
        'code': 200,
        'msg': '接收参数成功',
        'params': params
    }
    print('content1212:', content)
    response = JsonResponse(content)
    # 清空本地cookies
    response.delete_cookie('p03')
    response.delete_cookie('p09')
    response.delete_cookie('p10')
    response.delete_cookie('p12')
    return response


# 点赞接口(实时）
def share_like(request):
    '''
    先查询是否存在，不存在创建
    存在更新
    :param request:
    :return:
    '''
    if request.method != "GET":
        return JsonResponse({'code': 1, 'errmsg': '请使用GET提交'})
    share_id = request.GET.get('share_id',default='')
    islike = request.GET.get('islike')
    click_num = int(request.GET.get('click_num',default='1'))
    print('islike:{} type:{}'.format(islike,type(islike)))
    # click_num = 1
    defaults = {
        'shares_id': share_id,
    }
    fileparam = {'shares_id': share_id}
    share_obj = Shares.objects.filter(share_id=share_id)[0]
    try:
        Like_obj = LikeGrade.objects.get(**fileparam)
    except LikeGrade.DoesNotExist:
        base_score = share_obj.star
        like_num = click_num if islike == '1' else 0
        nlike_num = 0 if islike == '1' else click_num
        final_score = int((base_score + like_num) / (100 + like_num + nlike_num)*100)
        obj = LikeGrade.objects.create(shares_id=share_obj, base_score=base_score,
                                       like_num=like_num, nlike_num=nlike_num,
                                       final_score=final_score)
        share_obj.star = final_score
        share_obj.save()
        return JsonResponse({'code': 200, 'msg': "创建新记录成功！",'cur_score':final_score})

    if islike == '1':
        Like_obj.like_num = Like_obj.like_num + click_num
    else:
        Like_obj.nlike_num = Like_obj.nlike_num + 1
    Like_obj.final_score = int((Like_obj.base_score + Like_obj.like_num) / (100 + Like_obj.like_num + Like_obj.nlike_num)*100)
    Like_obj.save()
    share_obj.star = Like_obj.final_score
    share_obj.save()
    return JsonResponse({'code': 200, 'msg': "更新记录成功！",'cur_score':Like_obj.final_score})

    # 因为需要取原数据累加，不能放到一起使用
    # obj, created = LikeGrade.objects.update_or_create(shares_id = share_id)
