from django.contrib import admin

# Register your models here.

from .models import *

#注册该模型
@admin.register(Shares)
class SharesAdmin(admin.ModelAdmin):
    # 列表页面显示字段
    list_display = ['update_user_id', 'class_id','share_name','content']
    list_filter = ['share_name']
    list_per_page = 20
    ordering = ('update_user_id',)
    # 设置哪些字段可以点击进入编辑界面
    search_fields = ['share_name','content']
    list_display_links = ('update_user_id', 'share_name')


@admin.register(LikeGrade)
class LikeGradeAdmin(admin.ModelAdmin):
    # 列表页面显示字段
    list_display = ['like_grade_id', 'base_score','like_num','final_score','shares_id_id','nlike_num']
    # list_filter = ['shares_id_id']
    list_per_page = 20
    ordering = ('like_grade_id',)
    # 设置哪些字段可以点击进入编辑界面
    list_display_links = ('like_grade_id',)
