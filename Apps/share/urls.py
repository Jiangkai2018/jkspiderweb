from django.urls import path, re_path
from . import views

# /share/share_like/
urlpatterns = [
    path('getShareIndex/', views.getShareList, name='getShareList'),
    path('findShareListTable/', views.findShareListTable, name='findShareListTable'),
    path('getTabsDownList/', views.getTabsDownList, name='getTabsDownList'),
    path('getShareUpload/', views.getShareUpload, name='getShareUpload'),
    path('imageFileUpload/', views.imageFileUpload, name='imageFileUpload'),
    # 共享详情页
    re_path('getShareInfoPage/(\d+)/', views.getShareInfoPage, name='getShareInfoPage'),
    path('getUploadPage/', views.getUploadPage, name='getUploadPage'),
    path('fileUpload/', views.fileUpload, name='fileUpload'),
    # 文件信息上传
    path('uploadInfo/', views.uploadInfo, name='uploadInfo'),
    # 我的上传页面 getMyUploadList
    path('getMyUploadList/', views.getMyUploadList, name='getMyUploadList'),
    # 点赞
    path('share_like/', views.share_like, name='share_like'),
]
