# Generated by Django 2.1.3 on 2019-03-21 15:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('share', '0003_likegrade'),
    ]

    operations = [
        migrations.AddField(
            model_name='likegrade',
            name='shares_id',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='share.Shares'),
        ),
    ]
