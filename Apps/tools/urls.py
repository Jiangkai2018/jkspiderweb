from django.urls import path, re_path
from . import views

# /tools/getToolIndex/
urlpatterns = [
    path('', views.getTools, name='getTools'),
    path('getToolIndex/', views.getToolIndex, name='getToolIndex'),
    path('bsExce/', views.bsExce, name='bsExce'),
    path('getUaCls/', views.getUaCls, name='getUaCls'),
    path('findUAByCls/', views.findUAByCls, name='findUAByCls'),
    path('findRedisInfo/', views.findRedisInfo, name='findRedisInfo'),
    path('findSC/', views.findSC, name='findSC'),
    path('ocrPic/', views.ocrPic, name='ocrPic'),
    # re_path('(\d+)/', views.getArticleInfo, name='getArticleInfo'),
]
