from django.contrib import admin
from .models import *
# Register your models here.

@admin.register(UaClass)
class UaClassAdmin(admin.ModelAdmin):
    list_display = ['ua_class_id', 'ua_class_pid', 'ua_en_name','ua_cn_name']
    # list_per_page设置每页显示多少条记录，默认是100条
    list_per_page = 20
    ordering = ('ua_class_id',)
    # 设置哪些字段可以点击进入编辑界面
    search_fields = ['ua_en_name','ua_cn_name']
    list_display_links = ('ua_class_id',)


@admin.register(UaInfo)
class UaInfoAdmin(admin.ModelAdmin):
    list_display = ['ua_id', 'ua_name', 'ua_class','ua_value','is_delete']
    # list_per_page设置每页显示多少条记录，默认是100条
    list_per_page = 20
    ordering = ('ua_id',)
    # 设置哪些字段可以点击进入编辑界面
    search_fields = ['ua_name','ua_cn_name']
    list_display_links = ('ua_id','ua_name',)

@admin.register(SCInfo)
class SCInfoAdmin(admin.ModelAdmin):
    list_display = ['sc_id', 'sc_type', 'sc_type_name','sc_code','sc_en_name','sc_info']
    # list_per_page设置每页显示多少条记录，默认是100条
    list_per_page = 20
    ordering = ('sc_id',)
    # 设置哪些字段可以点击进入编辑界面
    search_fields = ['sc_code','sc_en_name']
    list_display_links = ('sc_id','sc_code',)

