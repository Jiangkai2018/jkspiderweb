# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class UaClass(models.Model):
    ua_class_id = models.IntegerField(primary_key=True)
    ua_class_pid = models.IntegerField(blank=True, null=True)
    ua_en_name = models.CharField(max_length=255, blank=True, null=True)
    ua_cn_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ua_class'


class UaInfo(models.Model):
    ua_id = models.AutoField(primary_key=True)
    ua_name = models.CharField(max_length=255, blank=True, null=True)
    ua_class = models.ForeignKey(UaClass, models.DO_NOTHING, blank=True, null=True)
    ua_value = models.CharField(max_length=1024)
    is_delete = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ua_info'


class SCInfo(models.Model):
    sc_id = models.AutoField(primary_key=True)
    sc_type = models.IntegerField(blank=False,null=False)
    sc_type_name = models.CharField(max_length=255,blank=False,null=False)
    sc_code = models.IntegerField()
    sc_en_name = models.CharField(max_length=100)
    sc_info = models.TextField()

    class Meta:
        db_table = "sc_info"