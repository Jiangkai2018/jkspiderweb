from django.forms import model_to_dict
from django.http import JsonResponse
from django.shortcuts import render
import re


from django.core import serializers

from Apps.userManage.views import getBaseInfo
from JKSpiderWeb.settings import *
# Create your views here.
from PIL import Image
# 获取相应工具页面 /tools/
from Apps.JkUtils.util_redis import RedisClient
from Apps.JkUtils.utils import util_bsExce
from Apps.tools.models import UaClass, UaInfo, SCInfo
try:
    import tesserocr
except ModuleNotFoundError:
    class tesserocr(object):
        def image_to_text(self,*args,**kwargs):
            return "ModuleNotFoundError: No module named 'tesserocr'"

def getTools(request):
    if request.method == "POST":
        return JsonResponse({"code": 4, "msg": "请使用GET方法访问"})
    type_id = request.GET.get('type', default='1')

    content = getBaseInfo(request)
    title = {'title':"在线工具"}
    content.update(title)
    page_name = "tools/tool_{}.html".format(type_id)
    return render(request, page_name,content)


# 工具主页
def getToolIndex(request):
    content = getBaseInfo(request)
    title = {'title':"在线工具主页"}
    content.update(title)
    return render(request,"tools/tool_index.html",context=content)



# BeautifulSoup 执行接口 post
def bsExce(request):
    # 对用户输入进行简单限制
    flist = ['\n', '\s', ':', ';']
    if request.method == "GET":
        return JsonResponse({"code": 4, "errmsg": "请使用POST方法访问"})
    # 参数
    html = request.POST.get('html')
    exceStr = request.POST.get('exceStr')

    if "bs." not in exceStr:
        return JsonResponse({"code": 3, "errmsg": "请以bs作为BeautifulSoup对象，例如 bs.title.string"})
    if not all([html, exceStr]):
        return JsonResponse({"code": 1, "errmsg": "参数不完整，请检查提交注册信息"})
    if any([x in exceStr for x in flist]):
        return JsonResponse({"code": 2, "errmsg": "可执行语句中存在不规范字符"})
    try:
        resData = util_bsExce(html, exceStr)
    except Exception as e:
        raise e
    if resData:
        return JsonResponse({"code": 200, "msg": "执行成功！", "resData": resData})
    else:
        return JsonResponse({"code": 5, "msg": "执行成功！,但查詢为空", "resData": resData})


# 获取UA下拉框类型接口
def getUaCls(request):
    if request.method == "POST":
        return JsonResponse({"code": 1, "errmsg": "请使用GET方法访问"})
    pid = request.GET.get('pid', default='')
    if not pid:
        return JsonResponse({"code": 2, "msg": "参数pid不存在"})

    res = UaClass.objects.filter(ua_class_pid=pid)
    res_json = serializers.serialize("json", res)
    if isinstance(res_json, str):
        res_json = eval(res_json)
    return JsonResponse({'findData': res_json})


# 查询某UA类型的UA
def findUAByCls(request):
    if request.method == "POST":
        return JsonResponse({"code": 1, "errmsg": "请使用GET方法访问"})
    clsId = request.GET.get('clsId', default='')
    findType = request.GET.get('type', default='0')
    # 随机查询UA
    if findType == "1":
        res = UaInfo.objects.order_by('?')[:10]
    else:
        if not clsId:
            return JsonResponse({"code": 2, "msg": "参数pid不存在"})
        res = UaInfo.objects.filter(ua_class_id=clsId)
    res_json = serializers.serialize("json", res)
    if isinstance(res_json, str):
        res_json = eval(res_json)
    static_info = {'code': 200, 'msg': '查询UA类型信息陈宫', 'clsId': clsId}
    resJson = {'findData': res_json}
    resJson.update(static_info)
    return JsonResponse(resJson)


# 随机获取代理IP
def findRedisInfo(request):
    if request.method == "POST":
        return JsonResponse({"code": 1, "errmsg": "请使用GET方法访问"})
    type = request.GET.get('type', default='0')
    num = request.GET.get('num', default='20')
    if type == '0':
        redis = RedisClient()
        countNum = redis.count()
        ipList = redis.random(int(num))
        return JsonResponse(
            {"code": 200, "msg": "请使用GET方法访问", 'countNum': countNum, 'ipLength': len(ipList), 'ipList': ipList})
    else:
        return JsonResponse({"code": 2, "msg": "无效参数"})


# 查询状态码
def findSC(request):
    if request.method == "POST":
        return JsonResponse({"code": 1, "errmsg": "请使用GET方法访问"})
    sc_code = request.GET.get('sc_value', default='')
    if not sc_code:
        return JsonResponse({"code": 2, "errmsg": "参数不能为空"})
    try:
        res = SCInfo.objects.get(sc_code=sc_code)
        cont = model_to_dict(res)
    except SCInfo.DoesNotExist:
        cont = {
        "sc_id": '未知',
        "sc_type": '未知类型',
        "sc_type_name": "未知类型",
        "sc_code": sc_code,
        "sc_en_name": "未知名",
        "sc_info": "<p>未查询到该状态信息，可能是输入错误的状态码信息，或者是状态码未收录入库</p>"
    }
    content = {"code": 200, "errmsg": "查询成功",'findData':cont}
    return JsonResponse(content)


# 验证码识别：

def ocrPic(request):
    if request.method == "POST":
        file_obj = request.FILES.get('file')
        filename = file_obj.name
        fileSize = file_obj.size
        if file_obj:
            res = JsonResponse({'msg:': '上传成功', 'code': 200})
        # 0 文件Logo，1 多文件截图列表 、 2 文件
        # 写入cookie:p03、p09、p10、p12
        _fzs = request.POST.get('fz_select')


        # 存放照片
        filepath = os.path.join(MEDIA_ROOT, OCRPIC_PATH, filename)
        path = os.path.join(MEDIA_ROOT, OCRPIC_PATH)
        # 路径不存在，新建
        if not os.path.exists(path):
            os.makedirs(path)
        # 如果存在相同名字的文件，删除
        if os.path.exists(filepath):
            os.remove(filepath)
        from django.core.files.storage import default_storage
        with default_storage.open(filepath, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)

        if _fzs == '1':
            # 灰度，二值化
            _fz = request.POST.get('fz_value')
            image = Image.open(filepath)
            image = image.convert('L')
            threshold = int(_fz)
            table = []
            for i in range(256):
                if i < threshold:
                    table.append(0)
                else:
                    table.append(1)
            image = image.point(table, '1')
            ocrRes = tesserocr.image_to_text(image)
        else:
            ocrRes = tesserocr.image_to_text(Image.open(filepath))
        res = {'code':200,'msg':'上传成功','filepath':filepath,'ocrResult':ocrRes}


        return JsonResponse(res)