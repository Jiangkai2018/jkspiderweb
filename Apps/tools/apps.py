from django.apps import AppConfig


class ToolsConfig(AppConfig):
    name = 'Apps.tools'
