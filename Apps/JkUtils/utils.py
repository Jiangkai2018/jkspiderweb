from lxml import etree
import re, bs4
from bs4 import BeautifulSoup
from requests import Request, Session
# 判断是否可迭代
from collections import Iterable

def paramDealNull(dicObj):
    '''
    dicObj = {'k1': 0, 'k2': '', 'k3': 23, 'k4': 'hello'}
    paramDealNull(dicObj)
    整合到findparam=dict()
    对参数为空的不整合到结果集合中
    :param:key (
    :return: dict()
    '''
    resDic = dict()
    for k in dicObj:
        print(k, dicObj[k])
        if isinstance(dicObj[k], list):
            if all(dicObj[k]):
                resDic[k] = dicObj[k]
            else:
                pass

        elif dicObj[k]:
            resDic[k] = dicObj[k]

    return resDic


def toFloat(ss):
    '''
    对例如'12.3MB'转换成xxx KB数值,不带'KB',用于文件大小比较和查询
    '''
    num = float(ss[:-2])
    if 'GB' in ss:
        num = num * 1024 * 1024
    if 'MB' in ss:
        num = num * 1024
    return num


def toBytes(ss):
    '''
    对例如'12.3MB'转换成xxx KB数值,不带'KB',用于文件大小比较和查询
    '''
    num = float(ss[:-2])
    if 'GB' in ss:
        num = num * 1024 * 1024 * 1024
    if 'MB' in ss:
        num = num * 1024 * 1024
    if 'KB' in ss:
        num = num * 1024
    return int(num)


def beUint(ss):
    '''
    将1024kb 转换成1MB
    :param ss:
    :return:
    '''
    print('beUint:{} '.format(ss))
    uType = 0
    if isinstance(ss,str):
        ss = int(ss)
    dicList = ['B', 'KB', 'MB', 'GB', 'TB']
    while ss >= 1024:
        ss = ss / 1024
        uType += 1
    return str(round(ss, 2)) + dicList[uType]


def parseUrl(filepath, rootNode):
    '''
    根据文件上传路径，以及跟节点，映射url相对地址
    :param filepath:
    :param rootNode:
    :return:
    '''
    filepath = filepath.replace(r'\\', '\\').replace('\\', '/')
    rootNode = rootNode.replace(r'\\', '\\').replace('\\', '/')
    rootNode = re.findall(r".*/(.*?)$", rootNode)
    if rootNode:
        pattern = r".*(/{}.*)$".format(rootNode)
        res = re.findall(pattern, filepath)
        return res[0]
    else:
        return ''


def util_bsExce(html, exceStr):
    bs = BeautifulSoup(html, 'lxml')
    res = eval(exceStr)
    if isinstance(res, bs4.element.Tag):
        res = str(res)
    if isinstance(res, Iterable):
        res = str(res)

    if isinstance(res, list):
        resp = ''
        if len(res)==1:
            return str(res[0])
        for x in res:
            resp += str(x) + '\n'
        res = resp
    return res


# 自定义爬虫功能函数
def dealCookieFormat(cookieStr):
    '''
    对浏览器的Cooke格式格式化成dict
    :param cookieStr:
    :return:
    '''
    cookielist = cookieStr.split(';')
    cookiedict = {}
    for x in cookielist:
        kv = x.split('=')
        cookiedict.update({kv[0].strip():kv[1].strip()})
    return cookiedict

def callSpider01(url,secURL = "",method ="GET",data = {},UA="",cookies = {},headers = {},proxies=None,timeout=5):
    '''
    返回获得原码html
    :param url:
    :param param:
    :return:
    '''
    if UA:
        headers.update({'User-Agent':UA})

    s = Session()
    try:
        r = s.request(url= url,method=method,data=data,headers=headers,cookies=cookies,proxies=proxies,timeout=timeout)
    except Exception:
        return "请求异常"
    if secURL:
        try:
            r = s.request(url=secURL, method='GET', data=data, headers=headers, cookies=cookies, proxies=proxies,
                 timeout=timeout)
        except Exception:
            return "第二个URL请求异常"
    return r.text


def utilDealHTML(html,parseType,parseRule):
    if isinstance(parseRule,str):
        parseRule = eval(parseRule)
    if parseType=='XPATH':
        htmlxpath = etree.HTML(html)
        res = {}
        for k in parseRule:
            res[k] =[k]+ htmlxpath.xpath(parseRule[k])
        evs = ''
        print('解析结果：',res)
        for k in res:
            evs += "res['{}'],".format(k)
        evs = evs[:-1]
        data = [list(x) for x in eval("zip({})".format(evs))]
        print('resData=', data)
        return data
    elif parseType=='RE':
        res = {}
        for k in parseRule:
            res[k] =[k]+ re.findall(parseRule[k],html)
        evs = ''
        for k in res:
            evs += "res['{}'],".format(k)
        evs = evs[:-1]
        data = [list(x) for x in eval("zip({})".format(evs))]
        print('resData=',data)
        return data
    else:
        return ['解析方法错误']
if __name__ == '__main__':

    # dicObj = {'k1': 0, 'k2': '', 'k3': 23, 'k4': 'hello', 'k5': ['', '1'], 'k6': [1, 2]}
    # print(paramDealNull(dicObj))

    # s = parseUrl(r'E:\个人文档\学业\毕业设计\毕设系统制作\JKSpiderWeb\media\upload_image\壁纸3.jpg',r'E:\个人文档\学业\毕业设计\毕设系统制作\JKSpiderWeb\media')
    # print(s)

    # print(beUint(55933))
    # print(toBytes('54.62KB'))

    # test
    # 自定义爬虫访问网站源码
    # cookies = dealCookieFormat("BAIDU_SSP_lcr=https://www.baidu.com/link?url=Wjc0KQ5OT6uctDRK7PIBQTgoVSIV44eAw8cW_KuUgiySIIcUTaQ7Isa88Lf9sFe-&wd=&eqid=fe2e232000104cb5000000025cad9ced; UM_distinctid=16976edfdb844c-07e68c96e56b93-594d2a16-144000-16976edfdb9382; BDTUJIAID=e7d1675ec4fd571011c7ee315927fed1; Hm_lvt_b88cfc1ccab788f0903cac38c894caa3=1553913934,1554004009,1554279668,1554369668; CNZZDATA2630830=cnzz_eid%3D625199672-1553237582-null%26ntime%3D1554877412")
    # contentg = callSpider01(url='http://httpbin.org/post',method='POST',data={'key1':'value'},UA='Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50',cookies=cookies)
    # print(contentg)

 #    res = utilDealHTML("""<div>
 #    <ul>
 #         <li class="item-0"><a href="link1.html">first item</a></li>
 #         <li class="item-1"><a href="link2.html">second item</a></li>
 #         <li class="item-inactive"><a href="link3.html">third item</a></li>
 #         <li class="item-1"><a href="link4.html">fourth item</a></li>
 #         <li class="item-0"><a href="link5.html">fifth item</a>
 #     </ul>
 # </div>""", 'XPATH', {'link':'//a/@href','name':'//a/text()','s':'//a'})
 #    print(res)

    res = utilDealHTML("""<div>
       <ul>
            <li class="item-0"><a href="link1.html">first item</a></li>
            <li class="item-1"><a href="link2.html">second item</a></li>
            <li class="item-inactive"><a href="link3.html">third item</a></li>
            <li class="item-1"><a href="link4.html">fourth item</a></li>
            <li class="item-0"><a href="link5.html">fifth item</a>
        </ul>
    </div>""", 'RE', {'link': '<a href="(.*?.html)">', 'name': '<a href=".*?.html">(.*?)</a>', 's': 'class="(.*?)">'})
    print(res)

    pass
