from django.urls import path, re_path
from . import views

# /diySpider/
urlpatterns = [
    path('getSpiderRequest/', views.getSpiderRequest, name='getSpiderRequest'),
    path('getSpiderParse/', views.getSpiderParse, name='getSpiderParse'),
    path('getURLHTML/', views.getURLHTML, name='getURLHTML'),
    path('dealHTML/', views.dealHTML, name='dealHTML'),
    # re_path('(\d+)/', views.getArticleInfo, name='getArticleInfo'),
]
