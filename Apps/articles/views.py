from django.forms import model_to_dict
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.safestring import mark_safe
# Create your views here.
from Apps.articles.models import Articles, collect_article
from Apps.userManage import user_login
from Apps.userManage.models import UserInfo
from django.core import serializers

import json
from django.core.serializers.json import DjangoJSONEncoder


# 页面加载
@user_login.login
def getArticleLists(request):
    user_id = request.session['user_id']
    userinfo = UserInfo.objects.get(pk=user_id)

    userinfojson = {
        "username": userinfo.username,
        "gender": userinfo.gender,
        "email": userinfo.email,
        "phone": userinfo.phone,
        "address": userinfo.address,
        "qq": userinfo.qq,
    }

    context = {'code': 1, "msg": "测试ok",
               'title': "学习网站——技术文章列表",
               "userinfo": userinfojson,
               }
    # return JsonResponse(context)  # test
    return render(request, 'articlesHtml/articleLists.html', context)


# 加载收藏列表页
# collectAritcle.html
@user_login.login
def getCArticleLists(request):
    user_id = request.session['user_id']
    userinfo = UserInfo.objects.get(pk=user_id)

    userinfojson = {
        "username": userinfo.username,
        "gender": userinfo.gender,
        "email": userinfo.email,
        "phone": userinfo.phone,
        "address": userinfo.address,
        "qq": userinfo.qq,
    }

    context = {'code': 1, "msg": "测试ok",
               'title': "学习网站——个人收藏页列表",
               "userinfo": userinfojson,
               }
    # return JsonResponse(context)  # test
    return render(request, 'articlesHtml/collectAritcle.html', context)


# 下拉列表接口 （作者名和来源）
def searchDropdownList(request):
    if request.method == "GET":
        findLists = request.GET.get('findLists', default='').split(',')
        pageType = request.GET.get('pageType', default='0')
        data = {}

        Ao = Articles.objects
        if pageType:
            userId = request.session['user_id']
            userName = request.session['username']
            articleIdList = collect_article.objects.filter(user_id=userId).values_list('articleid', flat=True)
            Ao = Ao.filter(pk__in=articleIdList)

        if len(findLists) > 0:
            # findLists :['author', 'source']
            for o in findLists:
                r = Ao.values(o).distinct().order_by(o)
                rs = []
                for key in r:
                    rs.append(key[o])
                # 方法二：
                # rs = json.dumps(list(r), cls=DjangoJSONEncoder)
                data.update({o: rs})

        context = {
            'code': 1, "msg": "测试ok",
            'data': data,
        }
        return JsonResponse(context)  # test

@user_login.login
def getArticleInfo(request, articleId):
    """
    获取文章详情页
    /article/1/
    :param request:
    article_id
    :return:
    """

    user_id = request.session['user_id']
    userinfo = UserInfo.objects.get(pk=user_id)

    userinfojson = {
        "username": userinfo.username,
        "gender": userinfo.gender,
        "email": userinfo.email,
        "phone": userinfo.phone,
        "address": userinfo.address,
        "qq": userinfo.qq,
    }

    articleInfo = Articles.objects.get(pk=articleId)

    articleInfoJson = {
        "articleid": articleInfo.articleid,
        "title": articleInfo.title,
        "publishtime": articleInfo.publishtime,
        "author": articleInfo.author,
        "source": articleInfo.source,
        "readnum": articleInfo.readnum,
        "contents": mark_safe(articleInfo.contents)
    }

    context = {'code': 1, "msg": "测试ok",
               'title': "学习网站——技术文章详情页",
               'articleId': articleId,
               "data": articleInfoJson,
               "userinfo": userinfojson
               }
    # return JsonResponse(context)  # test
    return render(request, 'articlesHtml/articleInfo.html', context)


def getArticleListsTable(request):
    """
    返回文章列表Json数据 多条件查询
    :param request:
    :return:
    """
    if request.method == "GET":
        title = request.GET.get('title', default='')
        author = request.GET.get('author', default='')
        source = request.GET.get('source', default='')
        publishtime_st = request.GET.get('publishtime_st', default='1970-01-01 00:00:00')
        publishtime_et = request.GET.get('publishtime_et', default='2099-01-01 00:00:00')
        readnum_sn = request.GET.get('readnum_sn', default=0)
        readnum_en = request.GET.get('readnum_en', default=99999)
        pageType = int(request.GET.get('pageType', default=0))

        # 模糊查询，包含__contains
        findparam = dict()
        if pageType:
            # 页面类型： 个人收藏，查询列表
            userId = request.session['user_id']
            userName = request.session['username']
            articleIdList = collect_article.objects.filter(user_id=userId).values_list('articleid', flat=True)
            findparam['pk__in'] = articleIdList
        if title:
            findparam['title__contains'] = title
        if author:
            findparam['author'] = author
        if source:
            findparam['source'] = source
        if publishtime_st and publishtime_et:
            # 范围查询
            # Record.objects.filter(end_time__range=[dt_s, dt_e])
            findparam['publishtime__range'] = [publishtime_st, publishtime_et]
        else:
            findparam['publishtime__range'] = ["1970-01-01 00:00:00", "2099-01-01 00:00:00"]
        if readnum_sn and readnum_en:
            findparam['readnum__range'] = [readnum_sn, readnum_en]
        # 多条件查询 关键点在这个位置传如的字典前面一定要加上两个星号.
        ArticlesListTables = Articles.objects.filter(**findparam)
        # 序列化
        # ArticlesListTablesInfo = [model_to_dict(Articles) for Articles in ArticlesListTables]
        ArticlesListTablesInfo = []
        if ArticlesListTables:
            for Article in ArticlesListTables:

                publishtime = str(Article.publishtime).replace("T", " ")
                if pageType:
                    ca = collect_article.objects.get(user_id=userId, articleid=Article.articleid)
                    publishtime = str(ca.collect_time).replace("T", " ")

                onedata = {
                    # "articleid": Article.articleid,
                    # "title": Article.title,
                    "titleLink": "<a href=\"{}\">{}</a>".format(Article.articleid, Article.title),
                    "publishtime": publishtime,
                    "author": Article.author,
                    "source": Article.source,
                    "readnum": str(Article.readnum)
                }
                ArticlesListTablesInfo.append(onedata)
        context = {'code': 1, "msg": "测试ok",
                   'title': "学习网站——技术文章列表",
                   "data": ArticlesListTablesInfo,
                   }
        return JsonResponse(context)  # test
        # return render(request, 'articlesHtml/articleLists.html', context)


# 设置收藏 -- 有则取消，无则收藏，通过初试化页面反显当前收藏情况
def setCollect(request):
    if request.method == 'GET':
        username = request.GET.get('username', default='')
        article_id = request.GET.get('articleid', default='')
        # 参数完整性校验
        if not all([username, article_id]):
            return JsonResponse({'code': 1, 'msg': '参数不完整'})
        userObj = UserInfo.objects.get(username=username)
        try:
            collectObj = collect_article.objects.get(articleid=int(article_id), user_id=userObj.id)
            # 取消收藏
            collectObj.delete()
            return JsonResponse({'code': 4, 'msg': '该文章收藏取消成功'})
        except collect_article.DoesNotExist:
            # 新建收藏
            articleObj = Articles.objects.get(articleid=int(article_id))
            if not articleObj:
                return JsonResponse({'code': 2, 'msg': '收藏文章id不存在'})
            dic = {'articleid': articleObj, 'user_id': userObj}
            collect_article.objects.create(**dic)
            return JsonResponse({'code': 3, 'msg': '收藏文章成功！'})


def checkCollect(request):
    if request.method == 'GET':
        username = request.GET.get('username', default='')
        article_id = request.GET.get('articleid', default='')
        # 参数完整性校验
        if not all([username, article_id]):
            return JsonResponse({'code': 1, 'msg': '参数不完整'})
        userObj = UserInfo.objects.get(username=username)
        try:
            collectObj = collect_article.objects.get(articleid=int(article_id), user_id=userObj.id)
            return JsonResponse({'code': 2, 'msg': '该文章已经收藏了'})
        except collect_article.DoesNotExist:
            return JsonResponse({'code': 3, 'msg': '该文章未收藏'})
