from django.contrib import admin
from .models import *
# Register your models here.


@admin.register(Articles)
class userAdmin(admin.ModelAdmin):

    list_display = ['articleid', 'title', 'publishtime', 'author', 'source', 'readnum', 'contents', 'url']
    list_filter = ['author', 'source']
    # list_per_page设置每页显示多少条记录，默认是100条
    list_per_page = 20
    ordering = ('articleid',)
    # 设置哪些字段可以点击进入编辑界面
    search_fields = ['title', 'contents']
    list_display_links = ('articleid', 'title')

@admin.register(collect_article)
class userAdmin(admin.ModelAdmin):
    list_display = ['articleid', 'user_id', 'collect_time']
    # list_per_page设置每页显示多少条记录，默认是100条
    list_per_page = 20
    ordering = ('articleid',)
    # 设置哪些字段可以点击进入编辑界面
    search_fields = ['user_id']
    list_display_links = ('articleid',)