from django.urls import path, re_path
from . import views

# /articles/getArticleListsTable/
urlpatterns = [
    path('', views.getArticleLists, name='getArticleLists'),
    path('getCArticleLists/', views.getCArticleLists, name='getCArticleLists'),
    # 多条件查询，模糊查询
    path('getArticleListsTable/', views.getArticleListsTable, name='getArticleListsTable'),
    path('setCollect/', views.setCollect, name='setCollect'),
    path('checkCollect/', views.checkCollect, name='checkCollect'),
    path('searchDropdownList/', views.searchDropdownList, name='searchDropdownList'),
    re_path('(\d+)/', views.getArticleInfo, name='getArticleInfo'),
]
