# coding=utf-8
from haystack import indexes
from Apps.articles.models import Articles

class ArticlesIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)  # 创建text字段
    # author = indexes.CharField(model_attr='author')  # char
    # title = indexes.CharField(model_attr='title')  # char

    def get_model(self):
        return Articles

    def index_queryset(self, using=None):
        return self.get_model().objects.all()