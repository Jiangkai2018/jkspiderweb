from django.db import models

# Create your models here.
from Apps.userManage.models import UserInfo


class Articles(models.Model):
    articleid = models.AutoField(db_column='articleId', primary_key=True)  # Field name made lowercase.
    title = models.CharField(max_length=255, blank=True, null=True)
    publishtime = models.DateTimeField(db_column='publishTime', blank=True, null=True)  # Field name made lowercase.
    author = models.CharField(max_length=255, blank=True, null=True)
    source = models.CharField(max_length=255, blank=True, null=True)
    readnum = models.IntegerField(db_column='readNum', blank=True, null=True)  # Field name made lowercase.
    contents = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        # 不自动根据模型类生成映射的数据库表
        managed = False
        db_table = 'articles'


# 文章收藏表
class collect_article(models.Model):
    articleid = models.ForeignKey(Articles, on_delete=models.CASCADE, default='')
    user_id = models.ForeignKey(UserInfo, on_delete=models.CASCADE, default='')
    collect_time = models.DateTimeField(auto_now_add=True,blank=True, null=True)
    class Meta:
        db_table = 'collect_article'



